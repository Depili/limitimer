package main

import (
	"bufio"
	"fmt"
	"github.com/tarm/serial"
	"gitlab.com/Depili/limitimer"
	"log"
	"os"
	"time"
)

const logTimestamp = "2006-01-02T15:04:05.000"

func main() {
	s := os.Args[1]
	log.Printf("Using serial device %s", s)

	logName := fmt.Sprintf("listener_log_%s.txt", time.Now().Format("2006-01-02T15:04:05"))
	rawName := fmt.Sprintf("listener_raw_%s.txt", time.Now().Format("2006-01-02T15:04:05"))

	f, err := os.Create(logName)
	if err != nil {
		log.Fatalf("Error creating logfile %s: %v", logName, err)
	}
	log.Printf("Writing to log: %s", logName)
	w := bufio.NewWriter(f)

	rf, err := os.Create(rawName)
	if err != nil {
		log.Fatalf("Error creating rawfile %s: %v", rawName, err)
	}
	log.Printf("Writing to raw: %s", rawName)
	raw := bufio.NewWriter(rf)

	c := &serial.Config{Name: s, Baud: 19200}

	port, err := serial.OpenPort(c)
	if err != nil {
		log.Fatalf("Error opening serial port %v", err)
	}

	buff := make([]byte, 100)
	decoder := limitimer.Decoder{}

	for {
		n, err := port.Read(buff)
		if err != nil {
			log.Fatalf("Error reading from serial port %v", err)
		}
		if n == 0 {
			log.Fatalf("EOF")
		}
		_, err = raw.Write(buff[0:n])
		if err != nil {
			log.Fatalf("Error writing to raw log: %v", err)
		}

		messages := decoder.Feed(buff[:n])
		for _, msg := range messages {
			switch limitimer.Type(msg) {
			case limitimer.StatusMsg:
				p, err := limitimer.ParseStateMessage(msg)
				if err != nil {
					m := fmt.Sprintf("Error parsing state packet: %v", err)
					log.Printf("%s", m)
					fmt.Fprintf(w, "%s %s\n", time.Now().Format(logTimestamp), m)
				} else {
					m := p.String()
					fmt.Fprintf(w, "%s %s\n", time.Now().Format(logTimestamp), m)
					m = " -> "
					for i := range p.Timers {
						hour, min, sec := p.TimerDisplay(i)
						m += fmt.Sprintf("P%d %0.2d:%0.2d:%0.2d ", i+1, hour, min, sec)
					}
					log.Printf("%s", m)
				}
			case limitimer.PingMsg:
				fmt.Fprintf(w, "%s -> PING\n", time.Now().Format(logTimestamp))
			default:
				fmt.Fprintf(w, "%s -> UNKNOWN %v\n", time.Now().Format(logTimestamp), msg)
			}
		}
	}
}
