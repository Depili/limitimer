package main

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/sigurn/crc16"
	"gitlab.com/Depili/limitimer"
	"io"
	"log"
	"os"
)

const (
	seeking    = 0
	startFound = iota
	checksum   = iota
	endFound   = iota
)

const (
	msgStart      = 0x81
	checksumStart = 0x83
	msgEnd        = 0xFF
)

var crcModbus *crc16.Table

type program struct {
	state      string
	totalMin   int
	totalSec   int
	sumupMin   int
	sumupSec   int
	elapsedMin int
	elapsedSec int
}

var msg10 = [6]byte{0x81, 0x10, 0x83, 0x89, 0x6d, 0xff}
var msg10zero = [6]byte{0x81, 0x10, 0x83, 0x00, 0x00, 0xff}
var interestingCounter [4]bool
var interestingState [4]bool

func main() {
	crcModbus = crc16.MakeTable(crc16.CRC16_MODBUS)

	file := os.Args[1]
	log.Printf("Opening %v", file)

	f, err := os.Open(file)
	if err != nil {
		panic(err)
	}
	br := bufio.NewReader(f)
	decoder := limitimer.Decoder{}

	for {
		data, err := br.ReadByte()
		if err != nil && !errors.Is(err, io.EOF) {
			log.Printf("Error: %v", err)
			break
		}

		messages := decoder.Feed([]byte{data})

		if len(messages) != 0 {
			for _, msg := range messages {
				switch limitimer.Type(msg) {
				case 0x00:
					decodeNormal(msg)
					decodeNew(msg)
				case 0x10:
					if len(msg) != 6 {
						log.Printf("Unknown 0x10 message!")
						outputRaw(msg)
					} else {
						invalid := false
						for i := range msg {
							if msg[i] != msg10[i] && msg[i] != msg10zero[i] {
								invalid = true
							}
						}
						if invalid {
							log.Printf("Unknown 0x10 message!")
							outputRaw(msg)
						} else {
							log.Printf("Ping")
						}

					}

				default:
					log.Printf("Unknown message type!")
					outputRaw(msg)
				}
			}
		}
		if err != nil {
			log.Printf("EOF")
			for i, s := range interestingCounter {
				if s {
					log.Printf("Interesting counters for P%d", i+1)
				}
			}

			for i, s := range interestingState {
				if s {
					log.Printf("Interesting states for P%d", i+1)
				}
			}

			break
		}
	}
}

func decodeNew(msg []byte) {
	var p limitimer.State

	err := p.Unmarshal(msg)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
	} else {
		fmt.Printf("%s\n", p.String())
	}
}

func decodeNormal(msg []byte) {
	expected := map[int]byte{
		0: 0x81, // Start of message, payload has bit 7 unset
		1: 0x00, // Message type
		// 2 & 3 - config dip switches
		//
		// Byte2:
		// 0x20 - Switch 9, Permit changes while running
		// 0x01 - Switch 8, beep type1
		//
		// Byte 3:
		// 0x40 - Switch 7, beep type2
		// 0x20 - Switch 6, beep volume
		// 0x10 - Switch 5, counting behaviour
		// 0x08 - Switch 4, counting direction
		// 0x04 - Switch 3, programs 1-3 count in hours
		// 0x02 - Switch 2, session (4) count in hours
		// 0x01 - Assumed switch 1, master/slave, always set in master mode
		//
		// 4 - some counter 0-9, seems to alternate between even/odd every other second
		// 5 - active program number
		// 6           - P1 state: 0 inactive, 1 running, ...?
		//               0x01 - Run
		//               0x02 - blink
		//               0x04 - beep
		//               0x08 - seconds adj
		// 7           - P1 seconds adjustment timeout, starts at 0x05 and counts down
		// 8, 9 & 10   - P1 total, in seconds (byte8*128*128 + byte9*128 + byte10)
		// 11, 12 & 13 - P1 sum up time
		// 14, 15 & 16 - P1 elapsed time
		// 17          - P2 state
		// 18          - P2 seconds adjustment timeout
		// 19, 20 & 21 - P2 total
		// 22, 23 & 24 - P2 sum up
		// 25, 26 & 27 - P2 elapsed
		// 28          - P3 state
		// 29          - P3 seconds adjustment timeout
		// 30, 31 & 32 - P3 total
		// 33, 34 & 35 - P3 sum up
		// 36, 37 & 38 - P3 elapsed
		// 39          - P4 state
		// 40          - P4 seconds adjustment timeout
		// 41, 42 & 43 - P4 total
		// 44, 45 & 46 - P4 sum up
		// 47, 48 & 49 - P4 elapsed
		50: 0x00,
		51: 0x83, // Start of checksum character
		// 52 & 53 - 16bit Modbus CRC, from bytes 0 to 51
		54: 0xFF,
	}
	if len(msg) != 55 {
		log.Printf("Invalid message lenght. Got %d expected 54.", len(msg))
		return
	}

	timers := [4]program{}

	raw := false

	for k, v := range expected {
		if msg[k] != v {
			log.Printf("Byte %d 0x%0.2X differs from expected value 0x%0.2X", k, msg[k], v)
			raw = true
		}
	}

	if msg[4] > 0x09 {
		log.Printf("Byte 4 out of known range: 0x%0.2X", msg[2])
		raw = true
	}

	if msg[5] > 0x3 {
		log.Printf("Byte 5 out of known range: 0x%0.2X", msg[5])
		raw = true
	}

	for i, b := range []byte{msg[6], msg[17], msg[28], msg[39]} {
		if b&0xF0 != 0 {
			interestingState[i] = true
			log.Printf("P%d state: 0x%0.2X", i+1, b)
		}
	}

	for i, b := range []byte{msg[7], msg[18], msg[29], msg[40]} {
		if b > 0x05 {
			log.Printf("P%d counter out of known range: 0x%0.2X", i, b)
			raw = true
		} else if b != 0 {
			interestingCounter[i] = true
		}
	}

	knownConfigHigh := ^byte(0x20 + 0x01)

	if msg[2]&knownConfigHigh != 0x00 {
		log.Printf("Unknown config high byte bits detected: 0x%0.2X", msg[2])
		raw = true
	}

	if msg[3]&0x01 != 0x01 {
		log.Printf("Config low byte bit 0 unset!")
		raw = true
	}

	if raw {
		outputRaw(msg)
		log.Fatalf("Unexpected data found!")
	}

	// Probably leds on the controller?
	state := "UNKNOWN"
	switch msg[5] {
	case 0:
		state = "Prog 1"
	case 1:
		state = "Prog 2"
	case 2:
		state = "Prog 3"
	case 3:
		state = "Prog 4"
	}

	for i := range timers {
		offset := 6 + (i * 11)
		switch msg[offset] {
		case 0:
			timers[i].state = "Off"
		case 1:
			timers[i].state = "Run"
		case 3:
			timers[i].state = "Blink"
		default:
			timers[i].state = fmt.Sprintf("0x%0.2X ", msg[offset])
		}

		timers[i].totalMin, timers[i].totalSec = parseTime(msg, offset+3)
		timers[i].sumupMin, timers[i].sumupSec = parseTime(msg, offset+6)
		timers[i].elapsedMin, timers[i].elapsedSec = parseTime(msg, offset+9)
	}

	crc := crc16.Checksum(msg[:52], crcModbus)
	msgChecksum := uint16(msg[52])<<8 + uint16(msg[53])

	if msgChecksum != crc && msgChecksum != 0 {
		log.Printf("Checksum error!")
		log.Printf("bytes %0.2X%0.2X uint16 %0.4X calculated %0.4X", msg[52], msg[53], msgChecksum, crc)
	}

	out := fmt.Sprintf(" -> %.6s (0x%0.2X)", state, msg[5])
	for i := range timers {
		t := timers[i]
		out += fmt.Sprintf("P%d %.5s %0.2d:%0.2d / %0.2d:%0.2d (%0.2d:%0.2d) ", i+1, t.state, t.elapsedMin, t.elapsedSec, t.totalMin, t.totalSec, t.sumupMin, t.sumupSec)
	}
	// log.Printf(out)

}

func parseTime(msg []byte, i int) (m int, s int) {
	t := parseDouble(msg, i)
	s = t % 60
	m = t / 60
	return
}

func parseDouble(msg []byte, i int) int {
	return int(msg[i])*128 + int(msg[i+1])
}

func outputRaw(msg []byte) {
	out := " -> "
	out += fmt.Sprintf(" [%02d] ", len(msg))
	for _, b := range msg {
		out += fmt.Sprintf("%0.2X ", b)
	}
	out = out[:len(out)-1]
	log.Printf(out)
}
