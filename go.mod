module gitlab.com/Depili/limitimer

go 1.17

require (
	github.com/sigurn/crc16 v0.0.0-20211026045750-20ab5afb07e3
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
)

require golang.org/x/sys v0.0.0-20211205182925-97ca703d548d // indirect
